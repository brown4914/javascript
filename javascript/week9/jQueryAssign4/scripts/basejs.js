$(function(){

// Onload Hide Event Details
$(function(){
    $(".event").children("ul").slideUp();
})

$(function(){
    $(".future_event").children("ul").slideUp();
})

//Hover Function for events
$(".clickable").hover(function() {
    $(this).css("color","lime");
   
  },function(){$(this).css("color", "black");} 
  );


//Parent Child Functions to hide or show event details

//User Clicks on Race Name

$(".event").click(function(){
    $(".event").children("ul").slideUp();
    //Select child li elements and slideToggle
    $(this).children("ul").slideToggle();

});

$(".future_event").click(function(){
    $(".future_event").children("ul").slideUp();
    //Select child li elements and slideToggle
    $(this).children("ul").slideToggle();

});

//Switch CSS style sheets and reveal future events, hide current events

$("#btnEvents").click(function(){
    var isFutureEvents = $("link[href='styles/future_style.css']");

    if (isFutureEvents.length){
        isFutureEvents.remove();
        $('#btnEvents').prop("value","Future Events");
    }else{
        $('head').append('<link type="text/css" rel="stylesheet" media="all" href="styles/future_style.css">');
        $('#btnEvents').prop("value","Current Events");
    }
});







});