$(function () {
//Buttons
    $("#walk").click(function(){
        loop();
    });

    $("#stop").click(function(){
        $(".walking").stop(true,false);
    });

    $("#rewind").click(function(){
        location.reload();
    });
//Functions
function loop(){
    $(".walking")
    .animate({backgroundPositionX:"-180",marginLeft:["+=10","linear"]},0)
    .delay(100)
    .animate({backgroundPositionX:"-350",marginLeft:["+=10","linear"]},0)
    .delay(100)
    .animate({backgroundPositionX:"-560",marginLeft:["+=10","linear"]},0)
    .delay(100)
    .animate({backgroundPositionX:"-755",marginLeft:["+=10","linear"]},0)
    .delay(100)
    .animate({backgroundPositionX:"-938",marginLeft:["+=10","linear"]},0)
    .delay(100)
    .animate({backgroundPositionX:"-1120",marginLeft:["+=10","linear"]},0)
    .delay(100)
    .animate({backgroundPositionX:"-1316",marginLeft:["+=10","linear"]},0)
    .delay(100)
    .animate({backgroundPositionX:"0",marginLeft:["+=10","linear"]},0,
    function(){
        loop();
    });
    
}

});
