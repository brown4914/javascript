// Array
var arrAnswers = new Array();
arrAnswers[0] ="Ask Again Later...";
arrAnswers[1] ="Yes";
arrAnswers[2] ="No";
arrAnswers[3] ="It appears to be so";
arrAnswers[4] ="Reply is hazy, please try again";
arrAnswers[5] ="Yes, definitely";
arrAnswers[6] ="What is it you really want to know?";
arrAnswers[7] ="Outlook is good";
arrAnswers[8] ="My sources say no";
arrAnswers[9] ="Signs point to yes";
arrAnswers[10] ="Don’t count on it";
arrAnswers[11] ="Cannot predict now";
arrAnswers[12] ="As I see it, yes";
arrAnswers[13] ="Better not tell you now";
arrAnswers[14] ="Concentrate and ask again";

//Fuctions


function Shake()
{
    var Answer = arrAnswers[ Math.floor(Math.random()*arrAnswers.length)];
    var Print = document.getElementById("lblAnswer").innerHTML = Answer;
    var Question = document.getElementById("txtInput").value;
    var lastChar = Question.substr(Question.length -1);
    //No Question Asked
    if(Question == "")
    {
        alert("Please ask me a question");
        document.getElementById("lblAnswer").innerHTML = "Please ask a question";
    }
    //No Question mark
    else if(lastChar != "?")
    {
        alert("Please use a question mark");
        document.getElementById("lblAnswer").innerHTML = "Please ask a question";
    }
    else
    {
        Print;
        document.getElementById("txtInput").value="";
    }
   
};