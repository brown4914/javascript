$(document).ready(function(){



    
    var count = 0;
    var countD = 13;
    var countC = 26;
    var countS = 39;
    function card(name,suit,value) {
        this.name = name;
        this.suit = suit;
        this.value = value;
    } 

    var deck = [
        new card('Ace', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Ace.jpg"),
        new card('Two', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Two.jpg"),
        new card('Three', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Three.jpg"),
        new card('Four', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Four.jpg"),
        new card('Five', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Five.jpg"),
        new card('Six', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Six.jpg"),
        new card('Seven', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Seven.jpg"),
        new card('Eight', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Eight.jpg"),
        new card('Nine', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Nine.jpg"),
        new card('Ten', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Ten.jpg"),
        new card('Jack', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Jack.jpg"),
        new card('Queen', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/Queen.jpg"),
        new card('King', 'Hearts',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Hearts/King.jpg"), //12

        new card('Ace', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Ace.jpg"), //13
        new card('Two', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Two.jpg"),
        new card('Three', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Three.jpg"),
        new card('Four', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Four.jpg"),
        new card('Five', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Five.jpg"),
        new card('Six', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Six.jpg"),
        new card('Seven', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Seven.jpg"),
        new card('Eight', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Eight.jpg"),
        new card('Nine', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Nine.jpg"),
        new card('Ten', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Ten.jpg"),
        new card('Jack', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Jack.jpg"),
        new card('Queen', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/Queen.jpg"),
        new card('King', 'Diamonds',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Diamonds/King.jpg"), //25

        new card('Ace', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Ace.jpg"), //26
        new card('Two', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Two.jpg"),
        new card('Three', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Three.jpg"),
        new card('Four', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Four.jpg"),
        new card('Five', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Five.jpg"),
        new card('Six', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Six.jpg"),
        new card('Seven', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Seven.jpg"),
        new card('Eight', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Eight.jpg"),
        new card('Nine', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Nine.jpg"),
        new card('Ten', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Ten.jpg"),
        new card('Jack', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Jack.jpg"),
        new card('Queen', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/Queen.jpg"),
        new card('King', 'Clubs',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Clubs/King.jpg"), //38

        new card('Ace', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Ace.jpg"), //39
        new card('Two', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Two.jpg"),
        new card('Three', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Three.jpg"),
        new card('Four', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Four.jpg"),
        new card('Five', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Five.jpg"),
        new card('Six', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Six.jpg"),
        new card('Seven', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Seven.jpg"),
        new card('Eight', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Eight.jpg"),
        new card('Nine', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Nine.jpg"),
        new card('Ten', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Ten.jpg"),
        new card('Jack', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Jack.jpg"),
        new card('Queen', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/Queen.jpg"),
        new card('King', 'Spades',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/cards/Spades/King.jpg") //51
    ];
    //Show Hearts
    function ShowHearts(){
        if(count<=12){
            $("<img>")
            .attr('src',deck[count].value)
            .appendTo("#heart")
            .hide()
            .fadeIn("fast")
            .delay("1000")
            .fadeOut("fast",function(){
                $("#heart").empty();
                count++;
                ShowHearts();
            });   
        }else{
            $("<img>")
            .attr('src',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/check_sm.png")
            .appendTo("#heart")
            .hide()
            .fadeIn("slow")
            .delay("1000")
            .fadeOut("slow",function(){
                $("#heart").empty(); 
                count = 0;
            })
         };
        
        
        
    }
    //Show Diamonds
    function ShowDiamonds(){
        
        if(countD<=25){
            $("<img>")
            .attr('src',deck[countD].value)
            .appendTo("#diamond")
            .hide()
            .fadeIn("fast")
            .delay("1000")
            .fadeOut("fast",function(){
                $("#diamond").empty();
                countD++;
                ShowDiamonds();
            });   
        }else{
            $("<img>")
            .attr('src',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/check_sm.png")
            .appendTo("#diamond")
            .hide()
            .fadeIn("slow")
            .delay("1000")
            .fadeOut("slow",function(){
                $("#diamond").empty(); 
                countD = 13;
            })
         };
        
        
        
    }
    //Show Clubs
    function ShowClubs(){
        
        if(countC<=38){
            $("<img>")
            .attr('src',deck[countC].value)
            .appendTo("#club")
            .hide()
            .fadeIn("fast")
            .delay("1000")
            .fadeOut("fast",function(){
                $("#club").empty();
                countC++;
                ShowClubs();
            });   
        }else{
            $("<img>")
            .attr('src',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/check_sm.png")
            .appendTo("#club")
            .hide()
            .fadeIn("slow")
            .delay("1000")
            .fadeOut("slow",function(){
                $("#club").empty(); 
                countC = 26;
            })
         };
        
        
        
    }
    //Show Spades
    function ShowSpades(){
        
        if(countS<=51){
            $("<img>")
            .attr('src',deck[countS].value)
            .appendTo("#spade")
            .hide()
            .fadeIn("fast")
            .delay("1000")
            .fadeOut("fast",function(){
                $("#spade").empty();
                countS++;
                ShowSpades();
            });   
        }else{
            $("<img>")
            .attr('src',"http://itweb.fvtc.edu/500004914/javascript/week10/Deal/Deal/images/check_sm.png")
            .appendTo("#spade")
            .hide()
            .fadeIn("slow")
            .delay("1000")
            .fadeOut("slow",function(){
                $("#spade").empty(); 
                countS = 39;
            })
         };
        
        
        
    }




//Click Functions
    $("#btnHeart").click(function(){
        ShowHearts();
    });
    $("#btnDiamond").click(function(){
        ShowDiamonds();
    });
    $("#btnClub").click(function(){
        ShowClubs();
    });
    $("#btnSpade").click(function(){
        ShowSpades();
    });

    
    
    
});  // end of ready