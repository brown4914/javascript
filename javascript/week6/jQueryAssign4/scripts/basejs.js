$(function(){
$(".races").hide();
//5K
$("#img5k").click(function(){
   $("#5k").toggle();
   if($("#half").is(':visible')||$("#full").is(':visible')){
    $("#half").slideUp();
    $("#full").fadeOut();
   }
});

//Half
$("#imgHalf").click(function(){
    $("#half").slideToggle();
    if($("#5k").is(':visible')||$("#full").is(':visible')){
        $("#5k").hide();
        $("#full").fadeOut();
       }
 });

//Full
 $("#imgFull").click(function(){
    $("#full").fadeToggle();
    if($("#half").is(':visible')||$("#5k").is(':visible')){
        $("#half").slideUp();
        $("#5k").hide();
       }

 }); 

});