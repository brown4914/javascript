//Document Ready
$(function(){
//$("#temp").text("<em>This is very cool!</em>");  
//Add html into an existing element.
    $("#temp").html("<em>This is very cool!</em>");    
    $("h1").html("<em>This is a very cool page!</em>"); 
//Single ticks on outside to let you use doubles inside. 
    $('<input type="button" value="Hide" id="btnShowHide" />').insertBefore("#temp");

//Click Event on button
    $("#btnShowHide").click(function(){
        $("p").toggle();
        if($("p").is(':visible')){
            $(this).val('Hide');
        }else{
            $(this).val('Show');
        }
    });

    //Add stuff to the beginning of the p tag
    $("div").append('<p>Append me to the list</p>.');

    //$("<strong>Hello </strong>").prependTo('p');
    $("<strong>Hello </strong>").prependTo('#temp');
    $("<em> good bye.</em>").appendTo('p');

    //$("p").remove();
    $("p").remove(':contains("Goodies")');
})
