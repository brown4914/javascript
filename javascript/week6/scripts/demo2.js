$(function(){
//Box 1
    $("#btnSlideUp").click(function(){
        $("#box1").slideUp();
    })
    $("#btnSlideDown").click(function(){
        $("#box1").slideDown();
    })
    $("#btnToggleSlide").click(function(){
        $("#box1").slideToggle();
    })
//Box 2
    $("#btnFadeOut").click(function(){
        $("#box2").fadeOut(3000);
    })
    $("#btnFadeIn").click(function(){
        $("#box2").fadeIn(3000);
    })
    $("#btnToggleFade").click(function(){
        $("#box2").fadeToggle('fast');
    })
    $("#btnFadeTo50").click(function(){
        $("#box2").fadeTo(1000,.5);
    })    
    $("#btnFadeTo100").click(function(){
        $("#box2").fadeTo(1000,1);
    })    
//Box 3
    $("#btnHide").click(function(){
        $("#box3").hide();
    })  
    $("#btnShow").click(function(){
        $("#box3").show();
    }) 
    $("#btnToggleHide").click(function(){
        $("#box3").toggle();
    }) 
});

